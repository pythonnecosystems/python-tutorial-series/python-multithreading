# Python 다중 스레드: 스레드, 잠금, 세마포어 <sup>[1](#footnote_1)</sup>

> <font size="3">Python에서 스레드화 모듈을 사용하여 동시 실행을 위한 스레드를 생성하고 사용하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./multithreading.md#intro)
1. [다중 스레드란 무엇이며 왜 사용할까?](./multithreading.md#sec_02)
1. [Python에서 스레드를 만들고 시작하는 방법](./multithreading.md#sec_03)
1. [Python에서 스레드 가입과 종료 방법](./multithreading.md#sec_04)
1. [Python에서 잠금을 사용하여 스레드 동기화 방법](./multithreading.md#sec_05)
1. [Python에서 세마포어를 사용하여 공유 리소스 액세스 제어 방법](./multithreading.md#sec_06)
1. [Python에서 다중 스레드 프로그램의 예외와 오류 처리 방법](./multithreading.md#sec_07)
1. [Python에서 다중 스레드 프로그램의 성능 측정과 개선 방법](./multithreading.md#sec_08)
1. [요약](./multithreading.md#summary)

<a name="footnote_1">1</a>: [Python Tutorial 41 — Python Multithreading: Thread, Lock, Semaphore](https://python.plainenglish.io/python-tutorial-41-python-multithreading-thread-lock-semaphore-cd87066b97a9?sk=369fbe14aacf1988fa2ce3405c210f63)를 편역하였다.
