# Python 다중 스레드: 스레드, 잠금, 세마포어

## <a name="intro"></a> 개요
Python 다중 스레드에 대한 이 포스팅에서는 Python에서 스레딩 모듈을 사용하여 동시 실행을 위한 스레드(thread)를 생성하고 사용하는 방법을 설명한다. 잠금(lock)과 세마포어(semaphore)를 사용하여 여러 스레드 간의 공유 리소스에 대한 액세스를 동기화하고 제어하는 방법도 설명한다.

그러나 첫째, multithreading이란 무엇이며 왜 그것을 사용하는가?

## <a name="sec_02"></a> 다중 스레드이란 무엇이며 왜 사용할까?
다중 스레드(multithreading)는 하나의 프로그램에서 여러 작업이나 프로세스를 동시에 실행하는 기술이다. 스레드는 프로세서 코어 상에서 실행되는 기본 실행 단위이다. 프로그램은 병렬로 실행되는 여러 개의 스레드를 가질 수 있으며, 동일한 메모리 공간과 리소스를 공유한다. Python 스레딩 모듈은 Python에서 스레드를 생성하고 관리하기 위한 상위 수준의 인터페이스를 제공한다.

멀티스레딩을 사용하는 이유는 무엇일까? Python에서 멀티스레딩을 사용하면 다음과 같은 이점이 있다.

- 여러 프로세서 코어를 활용하고 CPU의 노는 시간을 줄여 프로그램의 성능과 응답성을 향상시킨다.
- 파일 다운로드, 데이터 처리, 사용자 인터페이스 업데이트 등과 같이 여러 작업을 동시에 수행한다.
- 웹 서버, 채팅 애플리케이션, 게임 등 여러 요청 또는 이벤트를 동시에 처리할 수 있는 동시 어플리케이션을 생성한다.

그러나 멀티스레딩에는 다음과 같은 과제와 제한 사항도 수반된다.

- 동일한 데이터 또는 리소스를 액세스하는 여러 스레드의 동기화와 조정을 관리하여 데이터 손상, 교착 상태 또는 레이스 조건을 방지해야 한다.
- 스레드 중 어느 하나에서 발생할 수 있는 예외와 오류를 처리하고, 스레드의 적절한 종료와 정리를 보장해야 한다.
- 여러 스레드가 동시에 Python 바이트코드를 실행하지 못하게 하여 Python 스레드의 병렬성을 제한하는 메커니즘인 GIL(Global Interpreter Lock)을 다루어야 한다.

이 포스팅에서는 이러한 문제를 극복하고 Python에서 멀티스레드를 효과적으로 사용하는 방법을 학습할 것이다. Python에서 스레드를 만들고 시작하는 방법부터 시작하여 스레드 가입과 종료 방법, 잠금과 세마포어를 사용하여 스레드를 동기화하는 방법, 멀티스레드 프로그램에서 예외와 오류를 처리하는 방법, 멀티스레드 프로그램의 성능을 측정하고 개선하는 방법을 설명할 것이다.

Python에서 멀티쓰레딩에 뛰어들 준비가 되었나요? 시작해 봅시다!

## <a name="sec_03"></a> Python에서 스레드를 만들고 시작하는 방법
이 절에서는 스레딩(threading) 모듈을 사용하여 Python에서 스레드를 생성하고 시작하는 방법을 설명한다. 스레딩 모듈은 스레드를 생성하는 두 가지 방법을 제공한다.

- `Thread` 클래스를 사용하고 호출 가능한 객체(예: 함수)를 대상 인수로 전달한다.
- `Thread` 클래스를 하위 클래스화하고 `run` 메서드를 오버라이드(override)한다.

예를 들어 두 가지 메서드 사용 방법을 살펴보자.

### `Thread` 클래스 사용과 호출 가능한 객체 전달
이는 Python에서 스레드를 만드는 가장 간단한 방법이다. 스레딩 모듈을 임포트하여 `Thread` 클래스의 인스턴스를 만들고 호출 가능한 객체(예: 함수)를 대상 인수로 전달하기만 하면 된다. 호출 가능한 객체에 대한 모든 인수를 `args` 인수로 전달할 수도 있다. 예를 들어 이름을 인수로 받아 인사 메시지를 출력하는 `say_hello`라는 함수가 있다고 가정하자. 이 함수를 실행하는 스레드를 다음과 같이 만들 수 있다.

```python
# Import the threading module
import threading

# Define a function that takes a name as an argument and prints a greeting message
def say_hello(name):
    print("Hello, " + name + "!")

# Create a thread that executes the say_hello function with the argument "Alice"
thread1 = threading.Thread(target=say_hello, args=("Alice",))
# Start the thread
thread1.start()
```

그러면 `"Alice"`를 인수로 `say_hello` 함수를 실행할 (기본적으로) `thread1`이라는 이름의 스레드가 생성된다. 또한 `Thread` 클래스에 `name` 인수를 전달하여 스레드의 이름을 지정할 수 있다. 예를 들어, 다른 인수로 동일한 함수를 실행하는 다른 스레드를 생성하고 다음과 같이 `Greeting-Thread`라고 이름을 지정할 수 있다.

```python
# Create another thread that executes the say_hello function with the argument "Bob" and name it "Greeting-Thread"
thread2 = threading.Thread(target=say_hello, args=("Bob",), name="Greeting-Thread")
# Start the thread
thread2.start()
```

이렇게 하면 `"Bob"` 인수로 `say_hello` 함수를 실행할 `Greeting-Thread`라는 이름의 스레드가 생성된다. 스레드 객체의 `name` 속성을 사용하여 스레드의 name을 액세스할 수 있다. 예를 들어 다음과 같이 두 스레드의 name을 인쇄할 수 있다.

```python
# Print the names of the two threads
print(thread1.name)
print(thread2.name)
```

이는 다음과 같이 출력한다.

```
Thread-1
Greeting-Thread
```

스레드를 시작하면 main 스레드(main 프로그램을 실행하는 스레드)와 동시에 실행된다. 이는 main 스레드와 `say_hello` 함수에 일부 `print` 문을 추가하면 알 수 있다. 예를 들어, `say_hello` 함수를 다음과 같이 수정한다.

```python
# Modify the say_hello function to add some print statements
def say_hello(name):
    print("Thread " + threading.current_thread().name + " is running")
    print("Hello, " + name + "!")
    print("Thread " + threading.current_thread().name + " is done")
```

이렇게 하면 greeting 메시지 전후에 현재 스레드(함수를 실행하는 스레드)의 name이 출력된다. 그런 다음 main 스레드에 다음과 같은 `print` 문을 추가한다.

```python
# Add some print statements in the main thread
print("Main thread is running")
thread1.start()
thread2.start()
print("Main thread is done")
```

그러면 두 스레드를 시작하기 전에 `"Main thread is running"` (main 스레드가 실행 중) 메시지가 출력되고, 시작한 후에 `"Main thread is done"` (main 스레드가 완료됨) 메시지가 출력된다. 이제 전체 프로그램을 실행하고 출력을 확인하면 다음과 같은 출력을 얻을 수 있다.

```
Main thread is running
Thread Thread-1 is running
Thread Greeting-Thread is running
Hello, Alice!
Thread Thread-1 is done
Main thread is done
Hello, Bob!
Thread Greeting-Thread is done
```

보다시피 main 스레드와 두 스레드가 동시에 실행되며, 운영체제에 의한 스레드의 스케줄링에 따라 출력 순서가 달라질 수 있다. 메인 스레드가 `"Main thread is done"`이라는 메시지를 출력하기 전에 두 스레드가 끝날 때까지 기다리지 않는 것도 볼 수 있다. 이는 main 스레드와 두 스레드가 서로 독립적이며, 두 스레드의 상태에 관계없이 main 스레드를 빠져나간다는 것을 의미한다. 이는 기본적으로 두 스레드가 `daemon` 스레드이기 때문이다. daemon 스레드는 백그라운드에서 실행되는 스레드로 main 스레드의 빠져나가는 것을 막지 못한다. 스레드 객체의 daemon 속성을 이용하여 스레드가 daemon 스레드인지 확인할 수 있다. 예를 들어 다음과 같이 두 스레드의 daemon 상태를 출력할 수 있다.

```python
# Print the daemon status of the two threads
print(thread1.daemon)
print(thread2.daemon)
```

이는 다음과 같이 출력한다.

```
True
True
```

스레드를 시작하기 전에 daemon 속성을 True 또는 False로 설정하여 스레드의 daemon 상태를 변경할 수 있다. 예를 들어 두 번째 스레드를 시작하기 전에 다음 행을 추가하여 daemon이 아닌 스레드로 만들 수 있다.

```python
# Make the second thread a non-daemon thread
thread2.daemon = False
```

이것은 두 번째 스레드를 daemon 스레드가 아닌 것으로 만들 것이고, 이는 main 스레드가 끝날 때까지 나가는 것을 방지한다는 것을 의미한다. 프로그램을 다시 실행하고 출력을 관찰함으로써 이것을 확인할 수 있다. 다음과 같은 출력을 얻을 수 있다.

```
Main thread is running
Thread Thread-1 is running
Thread Greeting-Thread is running
Hello, Alice!
Thread Thread-1 is done
Main thread is done
Hello, Bob!
Thread Greeting-Thread is done
```

보다시피 첫 번째 스레드가 여전히 daemon 스레드임에도 불구하고 main 스레드는 두 번째 스레드가 끝나기를 기다렸다가 종료한다. 데몬 스레드가 아닌 스레드가 모두 종료되었을 때만 main 스레드가 종료되기 때문이다. 따라서 스레드의 daemon 상태를 설정할 때는 프로그램 종료와 정리에 영향을 미칠 수 있으므로 주의해야 한다.

### `Thread` 클래스 하위 클래싱(subclassing)과 실행 방법 오버라이딩(overriding)
Python에서 스레드를 만드는 또 다른 방법은 Thread 클래스를 서브클래스화하고 `run` 메서드를 오버라이드하는 것이다. `run` 메서드는 스레드의 진입점이며 스레드가 실행할 로직을 정의한다. Thread 클래스를 서브클래스화하려면 threading 모듈을 임포트하고 Thread 클래스에서 상속하는 새 클래스를 정의하고 `run` 메서드를 구현해야 한다. 예를 들어 숫자의 제곱을 인쇄하는 스레드를 만들고 싶다고 가정하자. `SquareThread`라는 Thread 클래스의 서브클래스를 다음과 같이 만들 수 있다.

```python
# Import the threading module
import threading

# Define a subclass of the Thread class that prints the square of a number
class SquareThread(threading.Thread):
    # Initialize the thread with a number attribute
    def __init__(self, number):
        # Call the superclass constructor
        threading.Thread.__init__(self)
        # Assign the number attribute
        self.number = number
    # Override the run method
    def run(self):
        # Print the square of the number
        print(self.number ** 2)
```

그러면 `Thread` 클래스로부터 상속되는 `SquareThread`라는 새로운 클래스가 정의된다. `__init__` 메서드는 숫자 속성을 가진 스레드를 초기화하고, superclass constructor를 호출한다. `run` 메서드는 숫자 속성의 제곱을 출력한다. `SquareThread` 클래스의 인스턴스를 만들고 숫자를 인수로 전달할 수 있다. 예를 들어 다음과 같이 5의 제곱을 출력하는 스레드를 만들 수 있다.

```python
# Create a thread that prints the square of 5
square_thread = SquareThread(5)
```

### 스레드 시작

```python
square_thread.start() 
```

이 코드를 실행하면 `SquareThread` 클래스를 사용하여 새 스레드를 만들고 `5`의 제곱을 출력한다.

## <a name="sec_04"></a> Python에서 스레드 결합과 종료 방법
이 절에서는 threading 모듈을 사용하여 Python에서 스레드를 결합(join)하고 종료하는 방법을 설명한다. 스레드의 가입과 종료는 스레드의 실행과 완료를 제어할 수 있는 중요한 작업이다.

### Python에서 스테드 결합 방법
스레드에 결합한다는 것은 main 스레드나 다른 스레드를 진행하기 전에 스레드가 끝날 때까지 기다리는 것을 의미한다. 이는 다음 단계로 넘어가기 전에 스레드가 작업을 완료했는지 확인하고 싶을 때 유용하다. 예를 들어, 인터넷에서 파일을 다운로드하는 스레드가 있고, 파일이 다운로드된 후에 파일을 처리하고자 한다고 가정해보자. 다운로드 스레드를 main 스레드에 결합시킬 수 있다. 그렇게 하면 main 스레드는 파일을 처리하기 전에 다운로드 스레드가 끝날 때까지 기다릴 것이다.

Python에서 스레드를 결합시키려면 스레드 객체의 `join` 메서드를 사용해야 한다. `join` 메서드는 스레드가 끝날 때까지 기다리는 시간을 지정하는 `timeout`이라는 선택적 인수를 취한다. 만약 timeout이 지정되지 않았거나 `None`인 경우 `join` 메서드는 스레드가 끝날 때까지 무한정 대기할 것이다. 만약 timeout이 지정되어 스레드가 timeout 내에 끝나지 않으면 `join` 메서드는 스레드가 끝날 때까지 기다리지 않고 돌아올 것이다.

Python에서 스레드에 결합하는 예를 들어보자. URL을 인수로 받아서 그 URL에서 파일을 다운로드하는 `download_file`이라는 함수가 있다고 하자. 이 함수를 실행하는 스레드를 다음과 같이 만들 수 있다.

```python
 Import the threading and requests modules
import threading
import requests

# Define a function that takes a URL as an argument and downloads the file from the URL
def download_file(url):
    # Get the file name from the URL
    file_name = url.split("/")[-1]
    # Send a GET request to the URL and save the response content as a binary file
    response = requests.get(url)
    with open(file_name, "wb") as file:
        file.write(response.content)
    # Print a message when the download is complete
    print("Downloaded " + file_name)
# Create a thread that executes the download_file function with the URL of a sample PDF file
download_thread = threading.Thread(target=download_file, args=("https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf",))
```

그러면 샘플 PDF 파일의 URL로 `download_file` 함수를 실행할 `Thread-1`이라는 이름의 스레드가 (기본적으로) 생성된다. 이 스레드를 main 스레드에 결합하려면 다음과 같이 join 메서드를 사용할 수 있다.

```python
# Start the thread
download_thread.start()
# Join the thread to the main thread
download_thread.join()
# Print a message when the join is complete
print("Joined " + download_thread.name)
```

이렇게 하면 `download_thread`가 시작되고 종료될 때까지 기다렸다가 `"Joined Thread-1"`이라는 메시지를 인쇄할 수 있다. 다음과 같이 `join` 메서드에 대한 타임아웃(예를 들어 10초)을 지정할 수도 있다.

```python
# Start the thread
download_thread.start()
# Join the thread to the main thread with a timeout of 10 seconds
download_thread.join(timeout=10)
# Print a message when the join is complete or the timeout is reached
if download_thread.is_alive():
    print("Timeout reached")
else:
    print("Joined " + download_thread.name)
```

이렇게 하면 `download_thread`가 시작되어 완료될 때까지 기다리거나 10초를 통과하는 것 중 어느 것이 먼저인지를 확인할 수 있다. `download_thread`가 10초 이내에 완료되면 `"Joined Thread-1"`이라는 메시지가 출력된다. `download_thread`가 10초 이내에 완료되지 않으면 `"Timeout reached"`라는 메시지가 출력되고 `download_thread`가 완료될 때까지 기다리지 않고 main 스레드를 계속 진행한다. 스레드가 실행 중이면 True, 또는 시작되지 않았으면 False를 반환하는 스레드 객체의 `is_alive` 메서드를 사용하여 스레드가 아직 살아 있는지 확인할 수 있다.

### Python에서 스테드 종료 방법
스레드를 종료한다는 것은 스레드가 작업을 완료하기 전에 실행을 중지하는 것을 의미한다. 이는 너무 오래 걸리거나 더 이상 필요하지 않은 스레드를 취소하고 싶을 때 유용하다. 예를 들어, 장기간 실행되는 계산을 수행하는 스레드가 있는데 사용자가 키를 누르면 이를 중지하고 싶다고 가정해보자. 신호를 보내거나 스레드에 중지를 알리는 플래그를 설정하면 계산 스레드를 종료할 수 있다.

그러나 Python에서 스레드를 종료하는 것은 스레드를 중지하는 내장된 메서드가 없기 때문에 스레드를 연결하는 것만큼 쉽지 않다. 스레드를 강제로 종료하면 데이터가 손상되거나 메모리가 누출되거나 프로그램의 상태가 일관되지 않을 수 있기 때문에  threading 모듈은 스레드 객체에 대한 `stop` 또는 `kill` 메서드를 제공하지 않는다. 따라서 스레드를 우아하고 안전하게 종료하기 위해서는 자신만의 논리를 구현해야 한다.

파이썬에서 스레드를 종료하는 일반적인 두 가지 방법이 있다.

- `Event` 객체를 사용하여 스레드를 중지하도록 신호를 보낸다.
- 중지할 스레드를 표시하기 위해 `boolean` 플래그를 사용한다.

예를 들어 두 방법을 모두 살펴보자.

### `Event` 객체를 사용하여 스레드에 중지 신호 보내기
`Event` 객체는 스레드 간의 통신에 사용될 수 있는 동기화 프리미티브이다. `Event` 객체는 `set` 상태와 `clear` 상태 두 가지를 가진다. 스레드는 `set` 또는 `clear` 메서드를 사용하여 `Event` 객체를 set 하거나 clear 할 수 있다. 스레드는 `wait` 메서드를 사용하여 `Event` 객체가 set 되기를 기다릴 수 있다. 그러면되 `Event` 객체가 set 되거나 timeout에 도달할 때까지 스레드를 차단할 수 있다.

`Event` 객체를 사용하여 스레드를 종료하려면 `Event` 객체를 생성하고 이를 인수로 스레드에 전달해야 한다. 스레드는 주기적으로 `Event` 객체의 상태를 확인하고 `Event` 객체가 설정되어 있으면 루프나 함수를 종료해야 한다. main 스레드나 다른 스레드에서 `Event` 객체를 설정하여 스레드를 중지하도록 신호를 보낼 수 있다. 예를 들어, `count_numbers`라는 함수를 가지고 있다고 가정해보자. 이 함수를 실행하는 스레드를 다음과 같이 만들 수 있다.

```python
# Import the threading and time modules
import threading
import time

# Define a function that takes an Event object as an argument and prints the numbers from 1 to 10 with a delay of 1 second
def count_numbers(event):
    # Initialize a counter variable
    counter = 1
    # Loop until the counter reaches 10 or the event is set
    while counter <= 10 and not event.is_set():
        # Print the counter value
        print(counter)
        # Increment the counter
        counter += 1
        # Sleep for 1 second
        time.sleep(1)
    # Print a message when the loop is done
    print("Counting is done")
# Create an Event object
stop_event = threading.Event()
# Create a thread that executes the count_numbers function with the stop_event as an argument
count_thread = threading.Thread(target=count_numbers, args=(stop_event,))
```

그러면 `stop_event`를 인수로 `count_numbers` 함수를 실행할 (기본적으로) `Thread-1`이라는 이름의 스레드가 생성된다. `count_numbers` 함수는 `stop_event`를 설정하지 않는 한 `1`초의 지연을 두고 `1`부터 `10`까지의 숫자를 출력한다. `stop_event`를 설정하려면 다음과 같이 `set` 메서드를 사용할 수 있다.

```python
# Start the thread
count_thread.start()
# Wait for 5 seconds
time.sleep(5)
# Set the stop_event
stop_event.set()
# Print a message when the event is set
print("Event is set")
```

이렇게 하면 `count_thread`가 시작되고 `stop_event`를 설정하기 전에 `5`초 동안 기다리게 된다. `stop_event`가 설정되면 `카운트 스레드`는 루프를 빠져나와 `"Counting done"`이라는 메시지를 촐력한다. 또한 count_thread를 main 스레드에 경합시킬 수도 있다.

## <a name="sec_05"></a> Python에서 잠금을 사용하여 스레드 동기화 방법
이 절에서는 threading 모듈을 사용하여 Python에서 스레드를 동기화하기 위해 잠금을 사용하는 방법을 설명한다. 잠금은 한 번에 하나의 스레드만 공유 자원을 액세스하거나 코드의 중요한 섹션을 실행할 수 있도록 하는 데 사용할 수 있는 동기화 프리미티브이다. 잠금은 여러 스레드가 동일한 데이터를 동시에 수정하거나 읽으려고 할 때 발생할 수 있는 데이터 손상, 교착 상태 또는 레이스 상황을 방지할 수 있다.

그러나 먼저 잠금이란 무엇이며 어떻게 작동할까?

잠금은 `locked` 상태와 `unlocked` 상태의 두 가지 상태를 갖는 객체이다. 스레드는 잠금이 unlocked될 때까지 스레드를 차단하는 잠금 객체의 `acuire` 메서드를 사용하여 잠금을 획득할 수 있다. 스레드는 잠금 객체의 `release` 메서드를 사용하여 잠금을 해제할 수 있으며, 이 방법은 잠금을 해제하고 다른 스레드가 이를 획득할 수 있도록 한다. 스레드는 잠금 객체의 `locked` 메서드를 사용하여 잠금이 해제되었는지 또는 해제되었는지를 확인할 수도 있으며, 잠금이 해제되면 `True`, 잠금이 해제되면 `False`를 반환한다.

Python에서 잠금을 사용하려면 threading 모듈을 가져와 `Lock` 클래스의 인스턴스를 생성해야 한다. 예를 들어 다음과 같이 lock 객체를 생성할 수 있다.

```python
# Import the threading module
import threading

# Create a lock object
lock = threading.Lock()
```

이렇게 하면 처음에 잠금이 해제된 `lock`이라는 이름의 lock 객체가 생성된다. 이 lock 개체를 공유 자원을 액세스하거나 코드의 중요 섹션을 실행해야 하는 스레드에 전달하고 `acquire`와 `release` 메서드를 사용하여 액세스를 제어할 수 있다. 예를 들어, lock 객체와 전역 변수 `counter`를 인수로 하고 카운터를 1씩 증가시키는 `increment_counter`라는 함수가 있다고 가정하자. 이 함수를 실행하는 스레드를 다음과 같이 두 개 만들 수 있다.

```python
# Define a global variable counter
counter = 0

# Define a function that takes a lock object and a global variable counter as arguments, and increments the counter by 1
def increment_counter(lock, counter):
    # Acquire the lock
    lock.acquire()
    # Increment the counter
    counter += 1
    # Print the counter value
    print(counter)
    # Release the lock
    lock.release()

# Create two threads that execute the increment_counter function with the lock and the counter as arguments
thread1 = threading.Thread(target=increment_counter, args=(lock, counter))
thread2 = threading.Thread(target=increment_counter, args=(lock, counter))
```

그러면 (기본적으로) `Thread-1`과 `Thread-2`라는 이름의 스레드 2개가 생성되며, 이 스레드는 `lock`과 `counter`를 인수로 사용하여 `increment_counter` 함수를 실행한다. `increment_counter` 함수는 `counter`를 증가시키기 전에 `lock`을 획득하고, `counter` 값을 출력한 후에 `lock`을 해제한다. 이렇게 하면 한 번에 하나의 스레드만 `counter`를 액세스할 수 있으며, `counter` 값의 불일치나 손상을 방지할 수 있다. 두 스레드를 시작하여 다음과 같이 출력을 볼 수 있다.

```python
# Start the two threads
thread1.start()
thread2.start()
```

이렇게 하면 두 스레드가 시작되고 각 증분 후 `counter` 값이 출력된다. 다음과 같은 출력을 얻을 수 있다.

```
1
2
```

보다시피 `counter` 값은 각 스레드에서 1씩 증가하며 최종 값은 2이다. `lock`으로 인해 두 스레드가 동시에 `counter`에 접근할 수 없으며, `counter` 값이 일관되고 정확하도록 보장하기 때문이다.

## <a name="sec_06"></a> Python에서 세마포어를 사용하여 공유 리소스 액세스 제어 방법
이 절에서는 threading 모듈을 사용하여 Python에서 공유 자원에 대한 액세스를 제어하는 세마포어(semaphore) 사용법을 설명한다. 세마포어는 한 번에 공유 자원에 액세스하거나 코드의 중요한 섹션을 실행할 수 있는 스레드의 수를 제한하는 데 사용할 수 있는 동기화 프리미티브이다. 세마포어는 너무 많은 스레드가 동일한 데이터에 동시에 액세스하거나 수정하려고 할 때 발생할 수 있는 데이터 손상, 교착 상태 또는 레이스 조건을 방지할 수 있다.

하지만 먼저, 세마포란 무엇이며 어떻게 작동할까?

세마포어는 사용 가능한 자원이나 슬롯의 수를 나타내는 카운터를 갖는 객체이다. 스레드는 카운터를 1씩 감소시키고 카운터가 양이 될 때까지 스레드를 차단하는 semaphore 객체의 `acquire` 메서드를 사용하여 세마포어를 획득할 수 있다. 스레드는 카운터를 1씩 증가시키고 다른 스레드가 세마포어를 획득하도록 하는 semaphore 객체의 `release` 메서드를 사용하여 세마포어를 해제할 수 있다. 스레드는 카운터의 현재 값을 반환하는 semaphore 객체의 값 속성을 사용하여 카운터의 값을 확인할 수 있다.

Python에서 세마포어를 사용하려면 threading 모듈을 임포트하여 `Semaphore` 클래스의 인스턴스를 생성해야 한다. 카운터의 초기값을 `Semaphore` 클래스의 인수로 지정하거나 비워 둘 수 있으며 기본값으로 1을 사용할 수 있다. 예를 들어 카운터가 `3`인 semaphore 객체를 다음과 같이 생성할 수 있다.

```python
# Import the threading module
import threading

# Create a semaphore object with a counter of 3
semaphore = threading.Semaphore(3)
```

그러면 카운터가 3인 `semaphore`라는 이름의 semaphore 객체가 생성된다. 이 semaphore 객체를 공유 자원을 액세스하거나 코드의 중요 섹션을 실행해야 하는 스레드에 전달하고 `acquire`와 `release` 메서드를 사용하여 액세스를 제어할 수 있습니다. 예를 들어, `print_number`라는 함수가 semaphore 객체와 숫자를 인수로 사용하고 숫자를 1초 지연하여 출력한한다고 가정해 보겠다. 이 함수를 실행하는 스레드를 다음과 같이 4개 만들 수 있다.

```python
# Import the time module
import time
# Define a function that takes a semaphore object and a number as arguments, and prints the number with a delay of 1 second
def print_number(semaphore, number):
    # Acquire the semaphore
    semaphore.acquire()
    # Print the number
    print(number)
    # Sleep for 1 second
    time.sleep(1)
    # Release the semaphore
    semaphore.release()
# Create four threads that execute the print_number function with the semaphore and different numbers as arguments
thread1 = threading.Thread(target=print_number, args=(semaphore, 1))
thread2 = threading.Thread(target=print_number, args=(semaphore, 2))
thread3 = threading.Thread(target=print_number, args=(semaphore, 3))
thread4 = threading.Thread(target=print_number, args=(semaphore, 4))
```

그러면 (기본적으로) `Thread-1`, `Thread-2`, `Thread-3`, `Thread-4`라는 이름의 4개의 스레드가 생성되며, 이 스레드는 `semaphore`와 다른 숫자를 인수로 사용하여 `print_number` 함수를 실행한다. `print_number` 함수는 숫자를 인쇄하기 전에 세마포어를 획득하고 1초 동안 sleep하고 나면 semaphore를 해제한다. 이렇게 하면 한 번에 세 개의 스레드만 `print_number` 함수를 액세스할 수 있으며, 네 번째 스레드는 다른 스레드 중 하나가 semaphore를 해제할 때까지 기다려야 한다. 4개의 스레드를 시작하여 다음과 같은 출력을 얻을 수 있다.

```python
# Start the four threads
thread1.start()
thread2.start()
thread3.start()
thread4.start()
```

이렇게 하면 네 개의 스레드가 시작되고 각 스레드의 숫자가 출력된다. 다음과 같은 출력을 얻을 수 있다.

```python
1
2
3
4
```

보다시피 4개의 숫자는 순서대로 인쇄되지만 각 숫자 사이에는 1초의 지연 시간이 존재한다. semaphore는 한 번에 3개의 스레드만 인쇄할 수 있도록 허용하고, 네 번째 스레드는 다른 스레드 중 하나가 semaphore를 해제할 때까지 기다려야 하기 때문이다. 이렇게 세마포어는 스레드의 동시성을 제한하여 공유 자원이나 코드의 임계 구간에 과부하가 걸리는 것을 방지한다.

## <a name="sec_07"></a> Python에서 다중 스레드 프로그램의 예외와 오류 처리 방법
이 절에서는 threading 모듈을 사용하여 Python에서 다중 스레드(multithrading) 프로그램의 예외와 오류를 처리하는 방법에 대해 설명한다. 어떤 프로그램에서도 예외와 오류는 피할 수 없으며 프로그램의 예기치 않은 동작이나 종료를 초래할 수 있다. 따라서 특히 예외와 오류가 여러 스레드와 프로그램의 전반적인 상태에 영향을 미칠 수 있는 다중 스레드 프로그램에서는 이를 적절하고 깔끔하게 처리하는 것이 중요하다.

그러나 먼저 예외와 오류는 무엇이며 어떻게 다른가?

예외는 프로그램의 실행 중에 발생하는 이벤트로서 프로그램의 정상적인 흐름을 방해한다. Python 인터프리터가 구문 오류, 런타임 오류 또는 프로그램의 논리적 오류에 직면했을 때 예외를 제기할 수 있다. 또한 프로그래머가 특별한 처리가 필요한 특정 조건이나 상황이 발생했음을 나타내기 위해 `raise` 문을 사용하여 예외를 제기할 수 있다. 예를 들어 `ZeroDivisionError` 예외는 Python 인터프리터가 숫자를 0으로 나누려고 할 때 발생하는데, 이는 잘못된 연산이다. `ValueError` 예외는 제곱근 함수에 음수를 전달하는 등 함수가 부적절한 값이나 타입의 인수를 받았을 때 프로그래머가 제기할 수 있다.

오류는 예외의 하위 클래스로 프로그램이 계속 실행되지 못하도록 하는 심각한 문제를 나타낸다. 일반적으로 Python 인터프리터가 메모리가 부족하거나, 존재하지 않는 파일을 액세스하거나, 사용할 수 없는 모듈을 가져오는 등의 시스템 관련 문제가 발생할 때 오류가 발생한다. 예를 들어, `MemoryError` 오류는 Python 인터프리터가 새 객체에 대해 충분한 메모리를 할당할 수 없을 때 발생하고, `FileNotFoundError`는 존재하지 않는 파일을 열려고 할 때 발생하며, `ImportError`는 Python 인터프리터가 사용할 수 없는 모듈을 가져오려고 할 때 발생한다.

예외와 오류의 차이점은 예외가 발생했을 때 프로그래머가 어떤 조치를 취할지를 지정할 수 있는 `try`, `except`, `else` 및 `final` 문을 사용하여 예외를 처리할 수 있다. 이를 통해 프로그래머는 예외 메시지를 표시하거나 예외를 기록하거나 예외를 복구할 수 있다. 반면에 오류는 프로그래머가 처리할 수 없으며, 일반적으로 상위 수준의 예외 처리자에게 잡히지 않는 한 프로그램이 갑자기 종료된다.

이제 예외와 오류가 무엇인지 알았으니 Python의 다중 스레드 프로그램에서 이를 처리하는 방법을 알아보자.

> **Note**: 기본적인 방법은 [Python 튜토리얼 18 — Python 예외 처리: Try, Except, Finally](https://pythonnecosystems.gitlab.io/python-tutorial-series/python-exception-handling)에서 설명하고 있다.

<span style="color:red">처리하는 방법이 기술되어 있지 않음</span>

## <a name="sec_08"></a> Python에서 다중 스레드 프로그램의 성능 측정과 개선 방법
이 절에서는 time 모듈과 multiprocessing 모듈을 사용하여 Python에서 멀티 스레드 프로그램의 성능을 측정하고 향상시키는 방법을 설명한다. 성능은 모든 프로그램, 특히 멀티 스레드 프로그램에서 프로세서 코어의 잠재력을 최대한 활용하고 작업의 실행 시간을 줄이고자 하는데 그 목적이 있다. 따라서 멀티 스레드 프로그램의 성능을 측정하는 방법과 더 나은 성능을 위해 최적화하는 방법을 아는 것이 유용하다.

하지만 먼저 성능이란 무엇이며 어떻게 측정할 수 있을까?

성능은 프로그램이 어떤 작업이나 기능을 얼마나 잘 수행하는지를 측정하는 것이다. 성능은 실행 시간, 메모리 사용량, CPU 사용량, 처리량, 대기 시간 등과 같은 다양한 메트릭으로 측정될 수 있다. 프로그램의 목표와 특성에 따라 다양한 성능 메트릭으로 프로그램을 최적화할 수 있다. 예를 들어, 프로그램이 클라이언트의 여러 요청을 처리하는 웹 서버인 경우, 프로그램이 초당 더 많은 요청을 처리하고 각 요청에 더 빠르게 응답할 수 있도록 프로그램을 높은 처리량과 낮은 대기 시간으로 최적화할 수 있다. 프로그램이 대규모 데이터 세트에서 복잡한 계산을 수행하는 데이터 분석 프로그램인 경우, 낮은 실행 시간과 메모리 사용량으로 프로그램을 최적화하고 싶을 수 있으며, 이는 프로그램이 계산을 더 빨리 완료하고 메모리 리소스를 더 적게 사용할 수 있음을 의미한다.

본 포스팅에서는 Python에서 프로그램이 작업이나 기능을 완료하는 데 걸리는 시간을 다중 스레드 프로그램의 실행 시간을 측정하고 개선하는 데 초점을 맞출 것이다. 실행 시간은 프로그램의 효율성과 속도를 반영하기 때문에 일반적이고 유용한 성능 지표이다. Python에서 다중 스레드 프로그램의 실행 시간을 측정하려면 시간 관련 데이터와 함께 작업할 수 있는 다양한 기능과 클래스를 제공하는 `time` 모듈을 사용하면 된다. 예를 들어 `time` 함수를 사용하여 현재 시간을 초 단위로 얻을 수도 있고, `perf_counter` 함수를 사용하여 짧은 지속 시간을 측정할 수 있는 고해상도 성능 카운터를 얻을 수도 있다. 작은 코드 조각의 실행 시간을 측정하는 간단한 방법을 제공하는 `timeit` 모듈을 반복 실행하고 평균 시간을 계산하여 사용할 수도 있다.

Python에서 다중 스레드 프로그램의 실행 시간을 측정하기 위해 `time`과 `timeit` 모듈을 사용하는 방법을 살펴보자. 어떤 수를 인수로 받고 그 수의 제곱근을 반환하는 `square_root`라는 함수가 있다고 하자. 이 함수를 실행하는 스레드를 다음과 같이 만들 수 있다.

```python
# Import the threading and math modules
import threading
import math

# Define a function that takes a number as an argument and returns the square root of the number
def square_root(number):
    # Return the square root of the number
    return math.sqrt(number)

# Create a thread that executes the square_root function with the number 25 as an argument
thread = threading.Thread(target=square_root, args=(25,))
```

그러면 숫자 `25`를 인수로 하는 `square_root` 함수를 실행할 `Thread-1` (기본적으로)이라는 이름의 스레드가 생성된다. 이 스레드의 실행 시간을 측정하기 위해 다음과 같이 `time` 함수를 사용할 수 있다.

```python
# Import the time module
import time

# Get the current time in seconds before starting the thread
start_time = time.time()
# Start the thread
thread.start()
# Join the thread to the main thread
thread.join()
# Get the current time in seconds after joining the thread
end_time = time.time()
# Calculate the execution time of the thread
execution_time = end_time - start_time
# Print the execution time of the thread
print("Execution time of the thread: " + str(execution_time) + " seconds")
```

그러면 스레드를 시작하고 합류하기 전과 합류한 후의 현재 시간을 초 단위로 얻고 종료 시간에서 시작 시간을 빼서 스레드의 실행 시간을 계산한다. 그러면 스레드의 실행 시간을 초 단위로 인쇄한다. 다음과 같은 결과를 얻을 수 있다.

```
Execution time of the thread: 0.0009999275207519531 seconds
```

보다시피, `square_root` 함수는 간단하고 빠른 계산이므로 스레드의 실행 시간은 매우 작다. 짧은 지속시간을 측정할 때 더 높은 해상도와 정확도를 제공하므로 시간 함수 대신 `perf_counter` 함수를 사용할 수도 있다. 예를 들어, 다음과 같이 코드를 수정할 수 있다.

```python
# Get the current performance counter before starting the thread
start_time = time.perf_counter()
# Start the thread
thread.start()
# Join the thread to the main thread
thread.join()
# Get the current performance counter after joining the thread
end_time = time.perf_counter()
# Calculate the execution time of the thread
execution_time = end_time - start_time
# Print the execution time of the thread
print("Execution time of the thread: " + str(execution_time) + " seconds")
```

이렇게 하면 스레드를 시작하고 합류하기 전과 합류한 후의 현재 성능 카운터를 얻을 수 있으며, 종료 시간에서 시작 시간을 빼서 스레드의 실행 시간을 계산한다. 그러면 스레드의 실행 시간을 초 단위로 인쇄한다. 다음과 같은 결과를 얻을 수 있다.

```
Execution time of the thread: 0.00010370000000000001 seconds
```

보다시피, `perf_counter` 함수가 보다 정밀한 측정을 제공하기 때문에 스레드의 실행 시간이 이전의 결과와 약간 다르다.

단일 함수 호출과 같이 작은 코드 스니펫의 실행 시간을 측정하고자 하는 경우 반복적으로 실행하고 평균 시간을 계산하여 작은 코드 스니펫의 실행 시간을 측정하는 간단한 방법을 제공하는 `timeit` 모듈을 사용할 수 있다. 예를 들어 `timeit` 모듈을 사용하여 숫자 `25`를 인수로 사용하여 다음과 같이 `square_root` 함수의 실행 시간을 측정할 수 있다.

```python
# Import the timeit module
import timeit

# Define a setup code that imports the math module and defines the square_root function
setup_code = """
import math
def square_root(number):
return math.sqrt(number)
"""

# Define a statement code that calls the square_root function with the number 25 as an argument
statement_code = "square_root(25)"
# Measure the execution time of the statement code by running it 100000 times and calculating the average time
execution_time = timeit.timeit(setup=setup_code, stmt=statement_code, number=100000)
# Print the execution time of the statement code
print("Execution time of the statement code: " + str(execution_time) + " seconds")
```

이것은 math 모듈을 가져와 `square_root` 함수를 정의하는 `setup_code`와 숫자 `25`인 `square_root` 함수를 인수로 호출하는 `statement_code`를 정의하고 있다. 그런 다음 `100000`번 실행하고 평균 시간을 계산하여 `statement_code`의 실행 시간을 측정할 것이다. 마지막으로 `statement_code`의 실행 시간을 초 단위로 출력할 것이다. 다음과 같은 결과를 얻을 수 있다.

```
Execution time of the statement code: 0.015726099999999998 seconds
```

보다시피 `statement_code`의 실행 시간은 `square_root` 함수를 100000번 실행한 평균 시간인데, 이 역시 `square_root` 함수가 단순하고 빠른 연산이기 때문에 매우 작다.

이제 Python에서 다중 스레드 프로그램의 실행 시간을 측정하는 방법을 알았으니, Python에서 다중 스레드 프로그램의 성능을 향상시키는 방법을 알아보자.

<span style="color:red">다중 스레드 프로그램의 성능을 향상시키는 방법이 기술되어 있지 않음</span>

## <a name="summary"></a> 요약
이 포스팅에서는 Python에서 threading 모듈을 사용하여 동시 실행을 위한 스레드를 생성하고 사용하는 방법을 학습하였다. 잠금과 세마포어를 사용하여 여러 스레드 간의 공유 자원에 대한 액세스를 동기화하고 제어하는 방법도 설명하였다. 멀티 스레드 프로그램에서 예외와 오류를 처리하는 방법, 멀티 스레드 프로그램의 성능을 측정하고 향상시키는 방법도 보였다(?).

이 포스팅에서 학습한 개념과 기법을 적용하면 파일 다운로드, 데이터 처리, 사용자 인터페이스 업데이트 등 여러 작업을 동시에 수행할 수 있는 보다 효율적이고 반응성이 뛰어난 프로그램을 만들 수 있다. 웹 서버, 채팅 애플리케이션, 게임 등 여러 요청이나 이벤트를 동시에 처리할 수 있는 동시 어플리케이션도 만들 수 있다.

그러나 멀티스레딩은 여러 스레드의 동기화와 조정 관리, 스레드에서 발생할 수 있는 예외와 오류 처리, Python 스레드의 병렬성을 제한하는 GIL(Global Interpreter Lock) 처리 등과 같은 과제와 제한도 수반한다. 따라서 Python에서 다중 스레드을 사용할 때는 주의하고 염두에 두어야 하며, 항상 다중 스레드 프로그램을 철저히 테스트하고 디버그해야 한다.
